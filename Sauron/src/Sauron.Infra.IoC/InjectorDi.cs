﻿using Microsoft.Extensions.DependencyInjection;
using Sauron.Application.AppService;
using Sauron.Application.Interfaces;
using Sauron.Data.Contexto;
using Sauron.Data.Repositorio;
using Sauron.Dominio.Interfaces;

namespace Sauron.Infra.IoC
{
    public class InjectorDi
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddDbContext<SauronContext>();

            services.AddScoped<IResponsavelRepositorio, ResponsavelRepositorio>();
            services.AddScoped<IResponsavelAppService, ResponsavelAppService>();
        }
    }
}