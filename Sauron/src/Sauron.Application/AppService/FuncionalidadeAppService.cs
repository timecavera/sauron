﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Sauron.Application.Interfaces;
using Sauron.Application.ViewModel.Funcionalidade;
using Sauron.Dominio.Interfaces;

namespace Sauron.Application.AppService
{
    public class FuncionalidadeAppService : IFuncionalidadeAppService
    {
        private readonly IFuncionalidadeRepository funcionalidadeRepository;

        public FuncionalidadeAppService(IFuncionalidadeRepository respository)
        {
            funcionalidadeRepository = respository;
        }

        public IEnumerable<DetalhesDaFuncionaliadeViewModel> ListarFuncionalidades(string sistema)
        {
            var listaDeFuncionalidades = funcionalidadeRepository.BuscarFuncionalidadesDoSistema(sistema);
            return FuncionalidadeFactory.ListarFuncionalidade(listaDeFuncionalidades);
        }

        public async Task AlterarFuncionalidade(AlterarFuncionalidadeViewModel alterarFuncionalidadeVm)
        {
            var funcionalidade = FuncionalidadeFactory.MapearFuncionalidade(alterarFuncionalidadeVm);
            await funcionalidadeRepository.AlterarFuncionalidade(funcionalidade);
        }

        public async Task CadastrarFuncionalidade(CadastrarFuncionalidadeViewModel cadastrarFuncionalidadeVm)
        {
            var funcionalidade = FuncionalidadeFactory.MapearFuncionalidade(cadastrarFuncionalidadeVm);
            await funcionalidadeRepository.AdicionarFuncionalidade(funcionalidade);
        }

        public async Task RemoverFuncionalidade(int id)
        {
            await funcionalidadeRepository.RemoverFuncionalidade(id);
        }
    }
}