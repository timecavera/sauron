﻿using Sauron.Application.Interfaces;
using Sauron.Application.ViewModel.Responsavel;
using Sauron.Dominio.Interfaces;

namespace Sauron.Application.AppService
{
    public class ResponsavelAppService : IResponsavelAppService
    {
        private readonly IResponsavelRepositorio responsavelRepositorio;

        public ResponsavelAppService(IResponsavelRepositorio responsavelRepositorio)
        {
            this.responsavelRepositorio = responsavelRepositorio;
        }

        public void RegistrarResponsavel(CadastrarResponsavelViewModel responsavelViewModel)
        {
            var responsavel = ResponsavelFactory.ConstruirResponsavel(responsavelViewModel);
            responsavelRepositorio.Adicionar(responsavel);
        }
    }
}