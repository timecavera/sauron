﻿using Sauron.Application.ViewModel.Responsavel;

namespace Sauron.Application.Interfaces
{
    public interface IResponsavelAppService
    {
        void RegistrarResponsavel(CadastrarResponsavelViewModel responsavelViewModel);
    }
}