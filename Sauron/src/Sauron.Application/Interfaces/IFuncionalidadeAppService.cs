﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Sauron.Application.ViewModel.Funcionalidade;

namespace Sauron.Application.Interfaces
{
    public interface IFuncionalidadeAppService
    {
        IEnumerable<DetalhesDaFuncionaliadeViewModel> ListarFuncionalidades(string sistemas);

        Task CadastrarFuncionalidade(CadastrarFuncionalidadeViewModel cadastrarFuncionalidadeVm);

        Task AlterarFuncionalidade(AlterarFuncionalidadeViewModel alterarFuncionalidadeVm);

        Task RemoverFuncionalidade(int id);
    }
}