﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Sauron.Application.ViewModel.Responsavel
{
    public class CadastrarResponsavelViewModel
    {
        [Required(ErrorMessage = "O celular é obrigatório.")]
        public string Celular { get; set; }

        [Required(ErrorMessage = "O Cpf é obrigatório.")]
        public string Cpf { get; set; }

        [Required(ErrorMessage = "O Email é obrigatório.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "O Nome é obrigatório.")]
        public string Nome { get; set; }

    }
}