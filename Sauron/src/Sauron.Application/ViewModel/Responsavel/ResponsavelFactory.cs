﻿namespace Sauron.Application.ViewModel.Responsavel
{
    public class ResponsavelFactory
    {
        public static Dominio.Classes.Responsavel ConstruirResponsavel(CadastrarResponsavelViewModel cadastrarResponsavelViewModel)
        {
            return new Dominio.Classes.Responsavel(cadastrarResponsavelViewModel.Nome, cadastrarResponsavelViewModel.Email, cadastrarResponsavelViewModel.Celular, cadastrarResponsavelViewModel.Cpf);
        }
    }
}