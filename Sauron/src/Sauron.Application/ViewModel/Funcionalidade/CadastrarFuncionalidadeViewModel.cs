﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Sauron.Application.ViewModel.Funcionalidade
{
    public class CadastrarFuncionalidadeViewModel
    {
        [DisplayName("Descrição da Funcionalidade")]
        [Required(ErrorMessage = "A descrição da funcionalidade é obrigatória.")]
        public string Descricao { get; set; }
    }
}