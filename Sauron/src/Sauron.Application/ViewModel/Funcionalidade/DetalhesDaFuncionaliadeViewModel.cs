﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Sauron.Application.ViewModel.Funcionalidade
{
    public class DetalhesDaFuncionaliadeViewModel
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [DisplayName("Descrição")]
        public string Descricao { get; set; }

        public bool Ativo { get; set; }
    }
}