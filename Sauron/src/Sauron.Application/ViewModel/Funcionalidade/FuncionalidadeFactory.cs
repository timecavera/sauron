﻿using System.Collections.Generic;
using System.Linq;

namespace Sauron.Application.ViewModel.Funcionalidade
{
    public class FuncionalidadeFactory
    {
        public static Dominio.Classes.Funcionalidade MapearFuncionalidade(CadastrarFuncionalidadeViewModel cadastrarFuncionalidadeVm)
        {
            return new Dominio.Classes.Funcionalidade(null, cadastrarFuncionalidadeVm.Descricao);
        }

        public static Dominio.Classes.Funcionalidade MapearFuncionalidade(AlterarFuncionalidadeViewModel alterarFuncionalidadeVm)
        {
            return new Dominio.Classes.Funcionalidade(alterarFuncionalidadeVm.Id, alterarFuncionalidadeVm.Descricao);
        }

        internal static IEnumerable<DetalhesDaFuncionaliadeViewModel> ListarFuncionalidade(IEnumerable<Dominio.Classes.Funcionalidade> listaDeFuncionalidades)
        {
            return listaDeFuncionalidades.Select(item => new DetalhesDaFuncionaliadeViewModel(){Ativo = item.Ativo, Descricao = item.Descricao}).ToList();
        }
    }
}