﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Sauron.Application.ViewModel.Funcionalidade
{
    public class AlterarFuncionalidadeViewModel
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [DisplayName("Descrição da Funcionalidade")]
        [Required(ErrorMessage = "A descrição da funcionalidade é obrigatória.")]
        public string Descricao { get; set; }
    }
}