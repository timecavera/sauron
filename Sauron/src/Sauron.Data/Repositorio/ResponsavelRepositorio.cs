﻿using Sauron.Data.Contexto;
using Sauron.Dominio.Classes;
using Sauron.Dominio.Interfaces;

namespace Sauron.Data.Repositorio
{
    public class ResponsavelRepositorio : IResponsavelRepositorio
    {
        private readonly SauronContext contexto;

        public ResponsavelRepositorio(SauronContext contexto)
        {
            this.contexto = contexto;
        }

        public void Adicionar(Responsavel responsavel)
        {
            contexto.Add(responsavel);
            contexto.SaveChanges();
        }
    }
}