﻿using Microsoft.AspNetCore.Mvc;
using Sauron.Application.Interfaces;
using Sauron.Application.ViewModel.Funcionalidade;
using System.Threading.Tasks;

namespace Sauron.Ui.Site.Controllers
{
    public class FuncionalidadeController : Controller
    {
        private readonly IFuncionalidadeAppService funcionalidadeAppService;

        public FuncionalidadeController(IFuncionalidadeAppService funcionalidadeAppService)
        {
            this.funcionalidadeAppService = funcionalidadeAppService;
        }

        public IActionResult Index(string sistemas)
        {
            return View(funcionalidadeAppService.ListarFuncionalidades(sistemas));
        }

        public IActionResult NovaFuncionaliade()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> NovaFuncionaliade(CadastrarFuncionalidadeViewModel cadastrarFuncionalidadeVm)
        {
            if (ModelState.IsValid)
            {
                await funcionalidadeAppService.CadastrarFuncionalidade(cadastrarFuncionalidadeVm);
                return RedirectToAction("Index");
            }
            return View(cadastrarFuncionalidadeVm);
        }

        public IActionResult AlterarFuncionaliade(int id)
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AlterarFuncionaliade(AlterarFuncionalidadeViewModel alterarFuncionalidadeVm)
        {
            if (ModelState.IsValid)
            {
                await funcionalidadeAppService.AlterarFuncionalidade(alterarFuncionalidadeVm);
                return RedirectToAction("Index");
            }
            return View(alterarFuncionalidadeVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoverFuncionaliade(int id)
        {
            await funcionalidadeAppService.RemoverFuncionalidade(id);
            return RedirectToAction("Index");
        }
    }
}