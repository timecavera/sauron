﻿using Microsoft.AspNetCore.Mvc;
using Sauron.Application.Interfaces;
using Sauron.Application.ViewModel.Responsavel;

namespace Sauron.Ui.Site.Controllers
{
    public class ResponsavelController : Controller
    {
        private readonly IResponsavelAppService responsavelAppService;

        public IActionResult NovoResponsavel()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult NovoResponsavel(CadastrarResponsavelViewModel responsavelViewModel)
        {
            if (ModelState.IsValid)
            {
                responsavelAppService.RegistrarResponsavel(responsavelViewModel);
                return RedirectToAction("Index");
            }
            return View(responsavelViewModel);
        }
    }
}