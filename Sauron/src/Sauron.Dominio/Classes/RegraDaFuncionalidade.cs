﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sauron.Dominio.Classes
{
    public class RegraDaFuncionalidade
    {
        protected RegraDaFuncionalidade()
        {

        }
        public int FunciondalideId { get; private set; }
        public int RegraId { get; private set; }
        public Funcionalidade Funcionalidade { get; set; }
        public Regra Regra { get; set; }
    }
}
