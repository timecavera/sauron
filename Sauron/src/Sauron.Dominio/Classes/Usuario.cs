﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sauron.Dominio.Classes
{
    public class Usuario
    {
        public Usuario(string cpf, string nome, string email, bool ativo)
        {
            this.Id = cpf;
            this.Nome = nome;
            this.Ativo = ativo;
            this.Email = email;
        }
        protected Usuario()
        {

        }
        public string Id { get; private set; }
        public string Nome { get; private set; }
        public string Email { get; private set; }
        public Guid Hash { get; private set; }
        public string Senha { get; private set; }
        public bool Ativo { get; private set; }
        public ICollection<PerfilDoUsuario> ListaDePerfilDoUsuario { get; private set; }
    }
}
