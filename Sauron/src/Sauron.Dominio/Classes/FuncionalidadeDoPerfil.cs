﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sauron.Dominio.Classes
{
    public class FuncionalidadeDoPerfil
    {
        protected FuncionalidadeDoPerfil()
        {

        }
        public int FunciondalideId { get; private set; }
        public int PerfilId { get; private set; }
        public Funcionalidade Funcionalidade { get; set; }
        public Perfil Perfil { get; set; }
    }
}
