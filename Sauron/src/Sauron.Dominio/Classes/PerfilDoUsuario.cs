﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sauron.Dominio.Classes
{
    public class PerfilDoUsuario
    {
        public PerfilDoUsuario(DateTime dataDeAssociacao, DateTime? dataDeExpiracao)
        {
            this.DataDeAssociacao = dataDeAssociacao;
            this.DataDeExpiracao = dataDeExpiracao;
        }
        protected PerfilDoUsuario()
        {

        }
        public int Id { get; private set; }
        public string UsuarioId { get; private set; }
        public int PerfilId { get; private set; }
        public DateTime DataDeAssociacao { get; private set; }
        public DateTime? DataDeExpiracao { get; private set; }
    }
}
