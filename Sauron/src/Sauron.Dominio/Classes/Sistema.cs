﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sauron.Dominio.Classes
{
    public class Sistema
    {
        public Sistema(string nome, string sigla, string url, int? idDaUnidadeOrcamentariaDoEestado)
        {
            this.Nome = nome;
            this.Sigla = sigla;
            this.Url = url;
            this.IdDaUnidadeOrcamentariaDoEestado = IdDaUnidadeOrcamentariaDoEestado;
        }
        protected Sistema()
        {

        }
        public int Id { get; private set; }
        public string Nome { get; private set; }
        public string Sigla { get; private set; }
        public string Url { get; private set; }
        public int IdDaUnidadeOrcamentariaDoEestado { get; private set; }
        public ICollection<Regra> ListaDeRegras { get; private set; }

    }
}
