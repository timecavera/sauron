﻿using System.Collections.Generic;

namespace Sauron.Dominio.Classes
{
    public class Funcionalidade
    {

        public Funcionalidade(int? id, string descricao)
        {
            this.Id = id ?? 0;
            this.Descricao = descricao;
            this.Ativo = true;
        }

        protected Funcionalidade()
        {}

        public int Id { get; private set; }

        public string Descricao { get; private set; }

        public bool Ativo { get; private set; }

        public ICollection<RegraDaFuncionalidade> ListaDeRegraDaFuncionalidades { get; private set; }
    }
}
