﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sauron.Dominio.Classes
{
    public class Regra
    {
        public Regra(string descricao, bool ativo)
        {
            this.Descricao = descricao;
            this.Ativo = ativo;
        }
        protected Regra()
        {

        }
        public int Id { get; private set; }
        public int SistemaId { get; private set; }
        public string Descricao { get; private set; }
        public bool Ativo { get; private set; }
        public Sistema Sistema { get; private set; }
        public ICollection<RegraDaFuncionalidade> ListaDeRegraDaFuncionalidades { get; private set; }
    }
}
