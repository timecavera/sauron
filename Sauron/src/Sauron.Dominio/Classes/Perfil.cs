﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sauron.Dominio.Classes
{
    public class Perfil
    {
        public Perfil(string descricao, bool ativo)
        {
            this.Descricao = descricao;
            this.Ativo = ativo;
        }
        protected Perfil()
        {

        }
        public int Id { get; private set; }
        public string Descricao { get; private set; }
        public bool Ativo { get; private set; }
        public ICollection<FuncionalidadeDoPerfil> ListaDeRegraDaFuncionalidades { get; private set; }
        public ICollection<PerfilDoUsuario> ListaDePerfilDoUsuario { get; private set; }
    }
}
