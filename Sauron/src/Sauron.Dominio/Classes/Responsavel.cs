﻿using System;

namespace Sauron.Dominio.Classes
{
    public class Responsavel
    {
        public Responsavel(string nome, string email, string celular, string cpf)
        {
            Nome = nome;
            Email = email;
            Celular = celular;
            Cpf = cpf;
        }

        protected Responsavel() { }

        public int Id { get; private set; }

        public string Celular { get; private set; }

        public string Cpf { get; private set; }

        public string Email { get; private set; }

        public string Nome { get; private set; }
    }
}