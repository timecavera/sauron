﻿using FluentValidation;
using System.Collections.Generic;
using FluentValidation.Results;

namespace Sauron.Dominio.Interfaces
{
    public interface IValidador<TEntity> where TEntity : class
    {
        IEnumerable<ValidationFailure> Erros();
        void AdicionarRegra(AbstractValidator<TEntity> regra);
        void AplicarPara(TEntity objeto);
        bool IsValid();
        void Dispose();
    }
}