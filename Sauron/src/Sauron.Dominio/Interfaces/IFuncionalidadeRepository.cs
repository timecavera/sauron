﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Sauron.Dominio.Classes;

namespace Sauron.Dominio.Interfaces
{
    public interface IFuncionalidadeRepository
    {
        IEnumerable<Funcionalidade> BuscarFuncionalidadesDoSistema(string sistema);

        Task AdicionarFuncionalidade(Funcionalidade funcionalidade);

        Task AlterarFuncionalidade(Funcionalidade funcionalidade);

        Task RemoverFuncionalidade(int id);
    }
}