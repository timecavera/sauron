﻿using Sauron.Dominio.Classes;

namespace Sauron.Dominio.Interfaces
{
    public interface IResponsavelRepositorio
    {
        void Adicionar(Responsavel responsavel);
    }
}